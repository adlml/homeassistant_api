from setuptools import setup, find_packages

setup(
  name = "homeassistant_api", 
  version = "0.0.5",
  url = "https://gitlab.com/hassbrain/homeassistant_api.git", 
  author = "Christian Meier", 
  author_email = "christian@meier-lossburg.de", 
  license = "MIT",
  packages = find_packages(), 
  install_requires = [ "asyncws" ]
)
